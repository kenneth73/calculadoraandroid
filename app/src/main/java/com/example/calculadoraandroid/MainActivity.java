package com.example.calculadoraandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText screenCalculadora;
    // O P E R A N D O S
    private Button btnsum;
    private Button btnRes;
    private Button btnDiv;
    private Button btnMul;
    private Button btnIgual;
    // PUNTO y 'C'
    private Button btnPoint;
    private Button btnC;
    //N U M E R O S
    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;

    private String text_screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //E N L A C E S
        screenCalculadora = findViewById(R.id.screenCalculadora);

        btnC =  findViewById(R.id.btnC);
        btnPoint = findViewById(R.id.btnPoint);
        btnDiv = findViewById(R.id.btnDiv);
        btnMul = findViewById(R.id.btnMul);
        btnRes = findViewById(R.id.btnRes);
        btnsum = findViewById(R.id.btnsum);
        btnIgual = findViewById(R.id.btnIgual);

        btn0 = findViewById(R.id.btn0);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        btn6 = findViewById(R.id.btn6);
        btn7 = findViewById(R.id.btn7);
        btn8 = findViewById(R.id.btn8);
        btn9 = findViewById(R.id.btn9);

        //L I S T E N E R S
        btnC.setOnClickListener(this);
        btnPoint.setOnClickListener(this);

        btnsum.setOnClickListener(this);
        btnRes.setOnClickListener(this);
        btnDiv.setOnClickListener(this);
        btnMul.setOnClickListener(this);
        btnIgual.setOnClickListener(this);

        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn0:
                text_screen = screenCalculadora.getText().toString();
                text_screen += "0";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btn1:
                text_screen = screenCalculadora.getText().toString();
                text_screen += "1";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btn2:
                text_screen = screenCalculadora.getText().toString();
                text_screen += "2";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btn3:
                text_screen = screenCalculadora.getText().toString();
                text_screen += "3";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btn4:
                text_screen = screenCalculadora.getText().toString();
                text_screen += "4";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btn5:
                text_screen = screenCalculadora.getText().toString();
                text_screen += "5";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btn6:
                text_screen = screenCalculadora.getText().toString();
                text_screen += "6";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btn7:
                text_screen = screenCalculadora.getText().toString();
                text_screen += "7";
                screenCalculadora.setText(text_screen);
            case R.id.btn8:
                text_screen = screenCalculadora.getText().toString();
                text_screen += "8";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btn9:
                text_screen = screenCalculadora.getText().toString();
                text_screen += "9";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btnDiv:
                text_screen = screenCalculadora.getText().toString();
                text_screen += " / ";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btnsum:
                text_screen = screenCalculadora.getText().toString();
                text_screen += " + ";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btnRes:
                text_screen = screenCalculadora.getText().toString();
                text_screen += " - ";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btnMul:
                text_screen = screenCalculadora.getText().toString();
                text_screen += " * ";
                screenCalculadora.setText(text_screen);
                break;
            case R.id.btnC:
                text_screen = "";
                screenCalculadora.setText("");
                break;
            case R.id.btnIgual:
                calcular(text_screen);
                break;
        }
    }
    public void calcular(String text_screen){
        int resultado;
        String[] array_text = text_screen.split(" ");

        if (array_text[1].equals('+')){
            resultado = Integer.parseInt(array_text[0]) + Integer.parseInt(array_text[2]) ;
        } else if (array_text[1].equals('-')){
            resultado = Integer.parseInt(array_text[0]) - Integer.parseInt(array_text[2]);
        }else if (array_text[1].equals('/')){
            resultado = Integer.parseInt(array_text[0]) / Integer.parseInt(array_text[2]);
        }else{
            resultado = Integer.parseInt(array_text[0]) * Integer.parseInt(array_text[2]);
        }
        String resultadoStr = String.valueOf(resultado);
        this.text_screen = "";
        screenCalculadora.setText(resultadoStr);
    }

}